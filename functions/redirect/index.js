// 云函数入口文件
const cloud = require('wx-server-sdk')
var https = require('https'); //http模块
cloud.init()
// 云函数入口函数
exports.main = async (event, context) => {
  var option = {
    hostname: '',
    path: ``,
    headers: {
      'Content-Type':'application/json',
      'Accept': '*/*',
      'Accept-Encoding': 'utf-8',  //这里设置返回的编码方式 设置其他的会是乱码
      'Accept-Language': 'zh-CN,zh;q=0.8',
      'Connection': 'keep-alive',
      'Referer': ''
    }
  };
  return new Promise((resolve, reject) => {
    try {
      https.get(option, (res) => {
        var html = '';
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
          html += chunk;
        })
        res.on('end', function () {
          return resolve(html)
        })
      })
    } catch (e) {
      return reject(e)
    }
  })
}