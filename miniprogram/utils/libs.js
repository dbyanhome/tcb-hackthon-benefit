const { stage, version } = require('../config.js');
import util from './util';
const oldPage = Page;
Page = function (app) {
  const oldOnLoad = app.onLoad;
  const oldOnShow = app.onShow;
  const oldOnHide =  app.onHide;
  const oldOnUnLoad = app.onUnLoad;
  app.onLoad = function (options) { // 这里必须使用function, 不可以使用箭头函数， 否则this指向错误
    if (typeof app.onLoad === 'function') {
      oldOnLoad && oldOnLoad.call(this, options); //apply
    }
  }
  app.onShow = function () { 
    // console.log("扩展onShow");
    if (typeof app.onShow === 'function') {
      oldOnShow && oldOnShow.call(this);
    }
  }

  app.onHide = function () {
    if (typeof app.onHide === 'function') {
      oldOnHide && oldOnHide.call(this);
    }
  }
  app.onUnLoad = function () {
    const that = this;
    if (that.rewardedVideoAd) {
      this.rewardedVideoAd.destroy()
    }
    if (typeof app.onUnLoad === 'function') {
      oldOnUnLoad && oldOnUnLoad.call(this);
    }
  }

  /**
   * 配置分享
   */
  if (!app.onShareAppMessage) {
    app.onShareAppMessage = function () {
      return {
        title: "众志成城,抗击肺炎!肺炎无情,人间有爱:",
        path: '/pages/index/index'
      }
    }
  }


  /**
   * 关闭悬浮跳转
   */
  app.onCloseFlotage = function () {
    const that = this;
    that.setData({
      flotageAtrr:!that.data.flotageAtrr
    })
  }

  return oldPage(app)
}