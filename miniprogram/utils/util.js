const { stage } = require('../config');
//保存图片到相册
function savePicToAlbum(tempFilePath) {
  wx.showLoading({
    title: '请稍后...',
    duration: 200000
  })
  if (!wx.saveImageToPhotosAlbum) {
    wx.hideLoading();
    wx.showModal({
      title: '提示',
      content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
    })
    return;
  }
  wx.getSetting({
    success(res) {
      wx.showLoading({
        title: '请稍后...',
        duration: 200000
      })
      if (!res.authSetting['scope.writePhotosAlbum']) {
        console.log("没有授权《保存图片》权限")
        wx.authorize({
          scope: 'scope.writePhotosAlbum',
          success() {
            console.log("授权《保存图片》权限成功")
            saveImageToPhotos(tempFilePath);
          },
          fail() {
            console.log("授权《保存图片》权限失败")
            wx.showModal({
              title: '提示',
              content: '请打开写入相册权限~',
              success: function () {
                wx.hideLoading();
                wx.openSetting({
                  success: function (res) {
                    if(res.authSetting['scope.writePhotosAlbum']){
                      saveImageToPhotos(tempFilePath);
                    }
                  },
                  fail: function (res) {
                    wx.showToast({
                      icon: 'none',
                      title: '打开设置失败,请重新设置',
                    });
                  }
                })
              }
            })
          }
        })
      } else {
        console.log("已经授权《保存图片》权限")
        saveImageToPhotos(tempFilePath);
      }
    }
  })
}

//保存图片
function saveImageToPhotos(tempFilePath) {
  wx.saveImageToPhotosAlbum({
    filePath: tempFilePath,
    success(res) {
      wx.hideLoading();
      wx.vibrateShort();
      wx.showToast({
        title: '保存成功',
        duration: 3000
      });
    },
    fail(err) {
      wx.hideLoading();
      console.log("失败原因：", err)
      wx.showToast({
        icon: 'none',
        title: '保存失败'
      });
    }
  })
}

/**
 * 区间随机数
 * @ minNum {*} 最大值
 * @ maxNum {*} 最小值
 */
function randomNum(minNum, maxNum) {
  switch (arguments.length) {
    case 1:
      return parseInt(Math.random() * minNum + 1, 10);
      break;
    case 2:
      return parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10);
      break;
    default:
      return 0;
      break;
  }
} 


/**
 * 校验图片
 * @ filePath {*} 图片路径
 */
async function checkImage(filePath) {
  return new Promise((reslove, reject) => {
    wx.getFileSystemManager().readFile({
      filePath,
      success: buffer => {
        //检查图片
        wx.cloud.callFunction({
          name: 'openapi',
          data: {
            action: 'imgSecCheck',
            value: buffer.data,
            contentType: 'image/png'
          },
          //验证通过
          success: function (res) {
            reslove(res.result.errCode)
          },
          fail: function (err) {
            reslove(err)
          }
        })
      }
    })
  })
}

/**
 * 校验文字
 * @ filePath {*} 校验文字
 */
async function checkFont(markFont) {
  return new Promise((reslove, reject) => {
    wx.cloud.callFunction({
      name: 'openapi',
      data: {
        action: 'msgSecCheck',
        content: markFont
      },
      success: res => {
        reslove(res.result.errCode)
      },
      fail: err => {
        reslove(err)
        wx.showToast({
          title: '此内容违反相关法律法规,请重新输入~',
          icon: 'none',
          duration: 3000
        })
      }
    })
  })
}

module.exports = {
  randomNum,
  checkImage,
  checkFont,
  savePicToAlbum
}
